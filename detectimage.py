import json
import os
import cv2 as cv
import numpy as np
from keras.models import model_from_json
from keras.preprocessing import image
import sys

#print("This is the name of the script: ", sys.argv[0])
#print("The arguments are: " , str(sys.argv))


#app.config["UPLOAD_FOLDER"] = "D:\kimDanAjYamProject\flask\uploads"

filePath = str(sys.argv[1])
#def load_images_from_folder(folder):
#    images = []
#    for filename in os.listdir(folder):
#        img = cv2.imread(os.path.join(folder,filename))
#        if img is not None:
#            images.append(img)
#    return images

def getEmotions():
    #load model
    model = model_from_json(open("fer.json", "r").read())
    #load weights
    model.load_weights('fer1hr2.h5')

    #model.predict(imgtest.reshape(-1,1))

    face_haar_cascade = cv.CascadeClassifier('haarcascade_frontalface_default.xml')

    cap= cv.imread(filePath)

    #captures frame and returns boolean value and captured image

    gray_img= cv.cvtColor(cap, cv.COLOR_BGR2GRAY)

    faces_detected = face_haar_cascade.detectMultiScale(gray_img, 1.32, 5)

    emotion_array = []
    emotion_count = []
    emotion_count_test = {}

    for (x,y,w,h) in faces_detected:
        cv.rectangle(cap,(x,y),(x+w,y+h),(255,0,0),thickness=7)
        roi_gray=gray_img[y:y+w,x:x+h]#cropping region of interest i.e. face area from  image
        roi_gray=cv.resize(roi_gray,(48,48))
        img_pixels = image.img_to_array(roi_gray)
        img_pixels = np.expand_dims(img_pixels, axis = 0)
        img_pixels /= 255
        predictions = model.predict(img_pixels)
#        print(predictions)
        #find max indexed array
        max_index = np.argmax(predictions[0])
#        print(max_index)
        emotions = ('angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral')
        predicted_emotion = emotions[max_index]
#        print(predicted_emotion)  
        emotion_array.append(predicted_emotion)
        cv.putText(cap, predicted_emotion, (int(x), int(y)), cv.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)

    resized_img = cv.resize(cap, (1000, 700))
    #cv.imshow('Facial emotion analysis ',resized_img)

    #total emotion in array
    len(emotion_array)
    emotion_array.append(len(emotion_array))
    #json.dumps(predicted_emotion)
    json_string = json.dumps(emotion_array)
#    print (json_string) 

    #count emotion and save in emotion_count  array
    emotion_count.append(json_string.count("happy"))
    emotion_count_test.update({'happy': json_string.count("happy")})
    emotion_count_test.update({'sad': json_string.count("sad")})
    emotion_count_test.update({'surprise': json_string.count("surprise")})
    emotion_count_test.update({'angry': json_string.count("angry")})
    emotion_count_test.update({'disgust': json_string.count("disgust")})
    emotion_count_test.update({'fear': json_string.count("fear")})
    emotion_count_test.update({'neutral': json_string.count("neutral")})
#    print(emotion_count_test)
    emotion_count_test.update({'total':sum(emotion_count_test.values())})
#    print('new emotions dict: ',emotion_count_test)
    return emotion_count_test

if __name__ == '__main__':
    result = getEmotions()
    print(result)
    result_json = json.dumps(result)
    file1 = open("myfile.txt","w+")
    file1.write(result_json)
    file1.close() 
    