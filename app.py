from flask import Flask , jsonify, request
import os
import json

#import detectimgae


app = Flask(__name__)


@app.route('/')
def home():
    return jsonify("home")


#@app.route('/test' , methods=['POST' , 'GET'])
#def predict():
#    import os 
#    import json
#    exe = "python detectimage.py"
#    os.system(exe)
#    file1 = open("myfile.txt","r+")
#    result = file1.read()
#    file1.close() 
#    return result
    
#app.config["UPLOAD_FOLDER"] = "D:\kimDanAjYamProject\flask\uploads"

@app.route("/upload", methods=["POST"])
def upload():
    import json
    import os.path, time
    import numpy as np
    from datetime import datetime
    result_list = []
    if request.method == "POST":
        files_dict = request.files.getlist('file')
#        booth_id = (file.filename)[0:3]
         
        for file in files_dict:
            file_name = file.filename
#            file_modificationDate = file.modificationDate
            filePathToSave = 'uploads/'+file_name
            file.save(filePathToSave)            
            result_dict =json.loads(predict(filePathToSave))
#            mock modificationtime แทน time stamp
#            timestamp = time.ctime(os.path.getctime(filePathToSave))
#            file_name='1574074018000.jpg'
            timestamp = datetime.fromtimestamp((int)(file_name[0:len(file_name)-4])/1000)
            result_dict['visitTime'] = timestamp
            result_list.append(result_dict)
            deleteFile(filePathToSave)
#    print(results)
#    
#    result_list=[{"happy": 2, "sad": 2, "surprise": 0, "angry": 0, "disgust": 0, "fear": 0, "neutral": 2, "total": 6, "visitTime": "Sun Nov 17 15:01:24 2019"}, {"happy": 4, "sad": 0, "surprise": 0, "angry": 0, "disgust": 0, "fear": 0, "neutral": 2,
#"total": 6, "visitTime": "Sun Nov 17 15:04:41 2019"}, {"happy": 0, "sad": 0, "surprise": 0, "angry": 0, "disgust": 0,
#"fear": 0, "neutral": 1, "total": 1, "visitTime": "Sun Nov 17 15:04:46 2019"},{"happy": 0, "sad": 0, "surprise": 0, "angry": 0, "disgust": 0,
#"fear": 0, "neutral": 1, "total": 1, "visitTime": "Sun Nov 17 16:04:46 2019"},{"happy": 0, "sad": 0, "surprise": 0, "angry": 0, "disgust": 0,
#"fear": 0, "neutral": 1, "total": 1, "visitTime": "Sun Nov 17 16:14:46 2019"}]
    summary_list = []
    tempTime = []
    for i in result_list:
#        visitTime = datetime.strptime(i['visitTime'],'%a %b %d %H:%M:%S %Y')
        visitTime = i['visitTime']
        iTime = [visitTime.year,visitTime.month,visitTime.day,visitTime.hour,0]
        if iTime in tempTime:
            dict_no = 0
            for item in np.arange(0,len(summary_list)):
                if summary_list[item]['visitTime'] == iTime:
                    dict_no = item
                    break
            summary_list[dict_no]["happy"] = (int)(summary_list[dict_no]["happy"])+ (int)(i["happy"])
            summary_list[dict_no]["sad"] = (int)(summary_list[dict_no]["sad"])+ (int)(i["sad"])
            summary_list[dict_no]["surprise"] = (int)(summary_list[dict_no]["surprise"])+ (int)(i["surprise"])
            summary_list[dict_no]["angry"] = (int)(summary_list[dict_no]["angry"])+ (int)(i["angry"])
            summary_list[dict_no]["disgust"] = (int)(summary_list[dict_no]["disgust"])+ (int)(i["disgust"])
            summary_list[dict_no]["fear"] = (int)(summary_list[dict_no]["fear"])+ (int)(i["fear"])
            summary_list[dict_no]["neutral"] = (int)(summary_list[dict_no]["neutral"])+ (int)(i["neutral"])
            summary_list[dict_no]["total"] = (int)(summary_list[dict_no]["total"])+ (int)(i["total"])
                    
        else:
            summary_dict={}
            summary_dict["happy"] = i["happy"]
            summary_dict["sad"] = i["sad"]
            summary_dict["surprise"] = i["surprise"]
            summary_dict["angry"] = i["angry"]
            summary_dict["disgust"] = i["disgust"]
            summary_dict["fear"] = i["fear"]
            summary_dict["neutral"] = i["neutral"]
            summary_dict["total"] = i["total"]
            summary_dict["visitTime"] = iTime
            summary_list = summary_list + [summary_dict]
            tempTime = tempTime + [[visitTime.year,visitTime.month,visitTime.day,visitTime.hour,0]]


    
    results2 = json.dumps(summary_list)  
    return results2


def predict(filePath):
    exe = "python detectimage.py "+filePath
    print(filePath)
    os.system(exe)
    file1 = open("myfile.txt","r+")
    result=file1.read()
    file1.close() 
    return result

def deleteFile(filePath):
    if os.path.exists(filePath):
        os.remove(filePath)
        print('deleted file ',filePath)
    else:
        print("The file does not exist")



if __name__ == "__main__":
    app.run(debug=True)
